// console.log("Hello World!");
// mini activity print your name in the console
// F12 or Ctrl + Shift + C to access the console
// console.log("Alpha Joy Cuaresma");
// Ctrl / for comments

//Section: Syntax, Statements, and Comments

//Statement in programming are instructions that we tell the computer to perform
	//usually ends with semicolon (;)
	//train us to locate where a statement ends
	//alert("Hello");
	//console.log("Hi");

//Syntax in programming, it is the set of rules that describes how statements must be constructed
//All lines/blocks of code should be written in a specific manner to work.
//This is due to how these codes were initially programmed to function and perform in a certain manner

//Where to Place JavaScript
	// Inline - you can place JS right into the HTML page using the script tags
		//for very small sites and testing only
		//the inline approach does not scale well, leads to poor organization, and code duplication
	//External - this is a better approach
		//place JS into separate files and link to them from the HTML page
		//this approach is much easier to maintain, write and debug

//Use of Script Tag
	//script tag can go anywhere on the page
	//as a best practice, many developers will place it just before the closing body tag on the HTML page.
	//This provides daster speed load times for our web page

//we use devtools also to DEBUG, view messages and run JavaScript code in the console tab

		console.log("Hello");
//whitespace can impact functionality in many computer languages-BUT not in JavaScript. In JS, whitespace is used only for readability and has no functional impact. One effect of this is a single statement that can span multiple lines.

		console.log("Hello World1");

		console. log("Hello World2 " ) ;

		console.
		log
		(
			"Hello World3"
		);

//Comments
	//comments are parts of the code that gets ignored by the language
	//Comments are meant to describe the written code

		/*
			There are two types of comments:
			1. The single-line comment denoted by two slashes (ctrl /)
			2. The multi-line comment denoted by a slash and asterisk (ctrl shift /)
		*/

//Variables
	//it is used to contain data
	//any information that is used by our applications are storede in what we call a memory
	//when we create varaibles, certain portions of a device's memory is given a "name" that we call "variables"

	//this makes it easier for us to associate information stored in our dewices to actual "names" about information

		let x = 1;
		let y;

	//Declaring variables
		//tells our devices that a variable name is created and is ready to store data
		//declaring a variable without giving it a value will automatically assign it with the value of "undefined" meaning that the variable's value is "not defined"

	//Syntax
		//let/const variableName;
		//let/const variableNameOne;

	//let is a keyword that is usually used in declaring a variable

		let myVariable;

		//console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console

		console.log(myVariable);//undefined

		//console.log(hello);//

		//variables must be declared first before they are used
		//using variables before they are declared will return an error
		let hello;

		/*
			Guidelines in writing variables:
			1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
			2. Variable names should start with a lowercase character, use camelCase for multiple words
			3. For Constant variables, use the 'const' keyword
			4. Variable names should be indicative or descriptove of the value being stored to avoid confusion

			Best practices in naming variables

			1.

			let firstName = "Michael";//good variable name
			let pokemon = "Charizard";//good variable name
			let pokemon = 25000;// bad variable name

			2.

			let FirstName = "Michael";//bad v name
			let lastName = "Jordan"; //good v name

			3.

			let first name = "Mickey"; //bad v name
			let firstName = "Mickey"; //good v name

			lastName emailAddress mobileNumber internetAllowance

			let product_description = "cool product";


		*/

//Declaring and initializing variables
	//Initializing variables - the instance when a varibale is given its initial/starting value

	//Syntax
		//let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 30000;
		console.log(productPrice);

		const interest = 3.539;


		// let- we usually use let if we want to reaasign the value in our variable

		// Reasign variable values
		// means changing its initial or previous value into 

		productName = 'Laptop';

		console.log(productName);

		// let friend = "kate";

		// friend = 'jane';

		let friend = "kate";
		// let friend = "jane";

		// interest = 4.4888;

		let supplier;

		supplier = 'john smith tradings';

		supplier = "Zuitt store";

		// console.log(supplier);

		// const pi;

		// pi = 3.1416;

		// console.log(pi)

		// let/const local global scope
		// Scope means where these variables are available to use
		// let and const are block scoped
		// a block lives in curly braces
		// anything within the curly braces is block 

		let outVariable = "hello";

		console.log(outVariable)

		{
			let innerVariable = "hello";

			console.log(innerVariable);
		}

		// console.log(innerVariable);

//Mulitiple variable declaration
// multiple variable may be declared in one line
// though it is qui

		let productCode = 'CD', productBrand = 'Dell';
		console.log(productCode,productBrand)

// const let = "hello";

// console.log(let);

// Section: Data Types
	// Strings
	// are a series of characters that creat a word, a phrase, a sentence or anything related to creating text
	// Strings in JS can be written using either a single (') or (")

		let country = "Philippines";
		let province = "mentro Manila";
	// Concatenationg strings
		//Multiple string values can ba conbined ato create a single string using the "+" symbol

		let fullAddress = province + ', ' + country;
		console.log(fullAddress);

		let greeting = 'I live the in '+ country;
		console.log(greeting);

		let mailAddress = 'Metro Manila\n\n\nPhilippines';
		console.log(mailAddress);
		let message = "john employees went home early";
		console.log(message);
// Number 
		let headCount =23;

		console.log(headCount);
		// decimal 
		let grade = 99.7;
		console.log(grade);
		let	planetDistance = 2e10;
		console.log(planetDistance)
		// combine strings sand #

		console.log("alpha " + grade);
		// Boolean values are normally to stor value relating to the state of certain things
		// this will be usefull in further discussion to certain scenarios

		let isMerried = false;
		let inGoodConduct= true;
		console.log('isMerried' + isMerried);
		console.log('isGoodConduct' + inGoodConduct);

		// Arrays are a spercial kind of data type that's used to store multiple data types

		let grades =[98.7,92.1,90.2,946];
		console.log(grades);

		let details = ["jhon", "smith", 32, true];
		console.log(details);

		// Objects 
		// Object or another special kind of data type tha's used to mimic real worl object/items
		// They are used to create data that contains pices of information that are relevant to each other 
		// every individual of piece of info is calles aproperty of the object

		let person = {
			fistName: 'Albedo',
			lastName: 'Cardo',
			age: 35,
			isMarried:false,
			contact: ["099999999", "098878687687"],
			address: {
				houseNumber: '248',
				city: 'Quezon city'
			}
		};

		console.log(person);

		// null
		// it is used to intentionally exzpress the absence os a value in a variable declaration/initialization
		let spouse = null;

		console.log(spouse)

		let myNumber = 0;
		let myString = "";


		// Undefine

		let fullName;

		console.log(fullName);