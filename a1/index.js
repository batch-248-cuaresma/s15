console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/
	let firstName ="First Name:" + " Alpha";
	console.log(firstName);
	let lastName = "Last Name:" + " Cuaresma";
	console.log(lastName);
	let myAge = "Age:" + " 27";
	console.log(myAge);
	let label = "Hobbies:";
	console.log(label);
	let hobbies = ["Rubiks","Online Games","Tiktok"];
	console.log(hobbies);
	let homeAddress = "Home Address:";
	console.log(homeAddress);
	let Arry = {
		houseNumber: '32',
		street: 'Washington',
		city:'Lincon',
		state: 'Nebraska',
	}
	console.log(Arry);

	let fullName = "Steve Rogers";
	console.log("My full name is" + ":" + fullName);

	let age = 40;
	console.log("My current age is: " + "currentAge");
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint" + "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		userName: "captain_america",
		full_Name: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let full_Name = "Bucky Barnes";
	console.log("My bestfriend is: " + full_Name);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
